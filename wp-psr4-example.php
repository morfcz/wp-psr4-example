<?php
/**
Plugin Name: PSR-4 for WordPress Example Implementation
Plugin URI: https://gitlab.com/morfcz/wp-psr4
Description: Example of PSR-4 autoloading implementation in plug-in.
Version: 1.0.0
Author: morfcz
Author URI: https://gitlab.com/morfcz/
Text Domain: wp-psr4-example
Domain Path: /languages
*/

// after plug-ins are loaded
add_action('plugins_loaded', function() {
    // get global instance of psr-4 autoloading class
    global $wpPsr4;

    if (!$wpPsr4 instanceof \WpPsr4\Psr4) {
        add_action('admin_notices', function() {
            printf(
                '<div class="notice notice-warning"><p>%s</p></div>',
                __(
                    'Plugin "PSR-4 for WordPress Example Implementation" cannot be initialized, because it requires "PSR-4 for WordPress" plug-in to be active.',
                    'wp-psr4-example'
                )
            );
        });

        return;
    }

    // add your namespace
    $wpPsr4->addNamespace('WpPsr4Example', __DIR__ . '/src/WpPsr4Example');
    // initiate your plugin
    WpPsr4Example\Plugin::init();
});
