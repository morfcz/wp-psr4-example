<?php

namespace WpPsr4Example;

class Plugin
{
    public static function init()
    {
        add_action('admin_notices', function() {
            printf(
                '<div class="notice notice-success"><p>%s</p></div>',
                __(
                    'Plugin "PSR-4 for WordPress Example Implementation" is running.',
                    'wp-psr4-example'
                )
            );
        });
    }
}
